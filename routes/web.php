<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// -------------------------------------------------
// HOME, SEARCH, CATEGORY
// -------------------------------------------------
Route::get('/', [PostController::class, 'homePageData'])->name('home');
Route::get('/comics/search', [PostController::class, 'search'])->name('post.search');
Route::get('/category/{name}', [PostController::class, 'showByCategory'])->name('category.show');

Route::prefix('MocoO')->group(function () {
    Route::get('/newest-post', [PostController::class, 'newestPosts'])->name('newest.posts');
    Route::get('/{comicSlug}/{encodePostId}', [PostController::class, 'showDetails'])->name('post.details');

    // CRUD Routes

    Route::middleware('auth')->group(function(){
        Route::get('/manage-blog', [PostController::class, 'index'])->name('blog.manage');
        Route::get('/add-post', [PostController::class, 'showAddPostForm'])->name('post.add');
        Route::post('/post.insert', [PostController::class, 'createPost'])->name('post.insert');
        Route::get('PostView/edit-post/{encodePostId}', [PostController::class, 'editPost'])->name('post.edit');
        Route::put('/update-post/{encodePostId}', [PostController::class, 'updatePost'])->name('post.update');
        Route::delete('/post/{id}', [PostController::class, 'destroy'])->name('post.destroy');
    });
});

// -------------------------------------------------
//              LOGIN AND REGISTER
// -------------------------------------------------
Route::get('/login', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('/login', [AuthController::class, 'login']);
Route::get('/register', [AuthController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [AuthController::class, 'register']);
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');