-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2023 at 09:49 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my-blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Life', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(2, 'Tutorial', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(3, 'Programming', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(4, 'Technology', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(5, 'Health', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(6, 'Travel', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(7, 'Food', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(8, 'Fashion', '2023-08-18 00:01:50', '2023-08-18 00:01:50'),
(9, 'Sports', '2023-08-18 00:01:50', '2023-08-18 00:01:50');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_07_21_074420_create_post', 1),
(6, '2023_07_27_100938_create_categories', 1),
(7, '2023_08_16_031219_post_category', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `image`, `slug`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Tales-of-Demons-and-Gods.jpg', 'MocoO-The King\'s Avatar', 'The King\'s Avatar', 'Doloremque rerum neque harum. Est quo molestias ut sit. Ut doloremque corporis ea dolores.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(2, 'wp10508800-manga-cover-wallpapers.jpg', 'MocoO-Solo Leveling', 'Solo Leveling', 'Consectetur veniam voluptate placeat. Aut iure voluptates hic nobis. Ipsum veniam natus velit aut.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(3, 'wp10508875-manga-cover-wallpapers.jpg', 'MocoO-Tales of Demons and Gods', 'Tales of Demons and Gods', 'Et eos ut fugit aliquam. Ad quisquam aut repellendus error magnam fuga voluptatem cumque. Reiciendis molestiae adipisci quia vel iusto unde rem.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(4, 'nano machine.jpg', 'MocoO-Versatile Mage', 'Versatile Mage', 'Vel doloremque sed omnis blanditiis officiis. Eos in sit molestias cumque quam labore. Cum deserunt dolorum velit omnis labore et harum.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(5, 'EapfzpZU8AUvNuU.jpg', 'MocoO-Naruto', 'Naruto', 'Id libero voluptatem voluptas quas. Animi accusantium error cumque cumque qui repellat. Doloribus eum debitis adipisci ut nobis.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(6, 'Hardcore_Leveling_Warrior_Webtoon_29.webp', 'MocoO-Tower of God', 'Tower of God', 'Deserunt vel dolores quasi. Deserunt perspiciatis odio enim. Autem sequi aut sed exercitationem quisquam.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(7, 'wp11656026-manga-covers-wallpapers.jpg', 'MocoO-Hardcore Leveling Warrior', 'Hardcore Leveling Warrior', 'Vel sint iste non inventore. Voluptatum autem non aperiam debitis est. Qui itaque eaque rerum labore dolorem sint et.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(8, 'Manhua_Cover_II.webp', 'MocoO-Naruto', 'Naruto', 'Et sunt ea tenetur magni nisi. Nostrum officiis ipsam maiores libero maxime fugiat sunt. Sed placeat dignissimos iste aut et.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(9, 'wp11656064-manga-covers-wallpapers.jpg', 'MocoO-Versatile Mage', 'Versatile Mage', 'Molestiae et debitis quia dolor est soluta. Magnam ut et non placeat mollitia vel animi. Vel fuga molestiae sunt sequi officiis quia repudiandae.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(10, 'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg', 'MocoO-The God of High School', 'The God of High School', 'Totam ut aliquam facilis magni. Est odio est nam natus quibusdam suscipit.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(11, 'solo leveling.png', 'MocoO-The King\'s Avatar', 'The King\'s Avatar', 'Quia nemo quis consectetur omnis sunt. In voluptates quibusdam mollitia. Quo aperiam vero quas non nulla nesciunt.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(12, 'wp11656089-manga-covers-wallpapers.jpg', 'MocoO-Tower of God', 'Tower of God', 'Quia libero expedita corporis. Est praesentium omnis qui non autem nemo placeat. Ea doloremque et quaerat vel necessitatibus architecto in.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(13, 'wp7988412-naruto-manga-art-wallpapers.jpg', 'MocoO-The King\'s Avatar', 'The King\'s Avatar', 'Molestias pariatur et possimus dolor assumenda eius vel in. Nobis sit eum deleniti velit laboriosam enim. Sequi facere iusto eos voluptates.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(14, 'Tales-of-Demons-and-Gods.jpg', 'MocoO-Sweet Home', 'Sweet Home', 'Quo soluta enim libero numquam. A porro fugiat maiores quae. Blanditiis quis pariatur consequuntur voluptatum distinctio aliquam sapiente.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(15, 'wp10508784-manga-cover-wallpapers.png', 'MocoO-Hardcore Leveling Warrior', 'Hardcore Leveling Warrior', 'Ipsa porro cum nulla molestiae non dolores qui aspernatur. Ipsum voluptatem amet enim quos. In sint ut a fugit modi at dignissimos incidunt.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(16, 'wp11656054-manga-covers-wallpapers.jpg', 'MocoO-Tower of God', 'Tower of God', 'Eum nemo ut impedit placeat temporibus exercitationem. Repudiandae vel aliquid et rerum. Quo non aut facilis voluptatem quibusdam. Et quia ab autem.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(17, 'wp11656131-manga-covers-wallpapers.jpg', 'MocoO-One Piece', 'One Piece', 'Iste vero consequuntur enim qui. Dicta sit occaecati et et aliquid harum doloremque. Id sapiente culpa occaecati et autem.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(18, 'wp10508875-manga-cover-wallpapers.jpg', 'MocoO-Attack on Titan', 'Attack on Titan', 'Dolores libero et est harum laboriosam et voluptatem mollitia. Nulla aut in quo quidem sed.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(19, 'solo leveling.png', 'MocoO-The God of High School', 'The God of High School', 'Et nemo autem temporibus quod aut. Iste maxime aperiam et eaque amet. Corporis dolorum qui minus temporibus eos.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(20, 'wp10508800-manga-cover-wallpapers.jpg', 'MocoO-My Hero Academia', 'My Hero Academia', 'Accusantium alias officiis fuga fugit vitae suscipit. Aut consectetur quis velit delectus et voluptates illo.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(21, 'wp11656131-manga-covers-wallpapers.jpg', 'MocoO-The King\'s Avatar', 'The King\'s Avatar', 'Aut quia temporibus magni necessitatibus omnis aut magnam. Eum similique ut aut eveniet. Quia vero numquam adipisci sapiente sit ut aut rerum.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(22, 'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg', 'MocoO-One Piece', 'One Piece', 'Quam ipsum nobis ratione cupiditate illo. Minima totam est libero sed ea omnis aut. Culpa culpa explicabo velit.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(23, 'evolution-begins-with-a-big-tree-1.webp', 'MocoO-The King\'s Avatar', 'The King\'s Avatar', 'Sunt atque magnam doloremque accusamus neque expedita mollitia. Est ducimus laudantium autem unde voluptas voluptas. Qui nihil quia non a vero.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(24, 'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg', 'MocoO-Solo Leveling', 'Solo Leveling', 'Quisquam vitae et placeat labore. Eos occaecati laborum sit qui ut corporis quaerat. Molestias culpa sit culpa vel.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(25, 'Donghua_Season_5_Poster.webp', 'MocoO-The King\'s Avatar', 'The King\'s Avatar', 'Vero nam inventore non eum est velit quo. Velit ducimus accusantium beatae voluptatem officiis voluptas. Dolore tempore nihil eum enim ut eligendi.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(26, 'EapfzpZU8AUvNuU.jpg', 'MocoO-The God of High School', 'The God of High School', 'Dolores quia quo aut et maiores. Eos qui saepe architecto suscipit et. Accusamus eos nisi suscipit sit consectetur asperiores eum.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(27, 'wp11656054-manga-covers-wallpapers.jpg', 'MocoO-Hardcore Leveling Warrior', 'Hardcore Leveling Warrior', 'Quod nobis aliquam itaque sapiente ullam est voluptas. Earum qui omnis magnam sit aut ea. Cumque hic beatae incidunt.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(28, 'wp10508787-manga-cover-wallpapers.jpg', 'MocoO-Hardcore Leveling Warrior', 'Hardcore Leveling Warrior', 'Sunt dolor cupiditate ut unde. Repellat et aut quidem perferendis fugit soluta. Totam ut accusamus et recusandae eveniet sequi.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(29, 'wp10508787-manga-cover-wallpapers.jpg', 'MocoO-The God of High School', 'The God of High School', 'Dolores recusandae omnis saepe. Dolores possimus perferendis molestias deserunt. Sed excepturi deserunt corrupti ex nihil quis ex.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(30, 'wp7988412-naruto-manga-art-wallpapers.jpg', 'MocoO-Versatile Mage', 'Versatile Mage', 'Rerum aut non voluptate enim. Ut consequatur nihil ex ut architecto esse. Quisquam pariatur est qui.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(31, 'overgeared.jpg', 'MocoO-Naruto', 'Naruto', 'Quia voluptatibus est laudantium perferendis nisi. Voluptas harum qui consequuntur. Neque eos autem aut odit.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(32, 'wp11656054-manga-covers-wallpapers.jpg', 'MocoO-Martial Peak', 'Martial Peak', 'Rerum molestias et eos maiores. Ea qui fugit aut et sit est. Est aut et vel ab nam. Quia sit expedita in voluptatum nemo dolor.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(33, 'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg', 'MocoO-Versatile Mage', 'Versatile Mage', 'Animi aut et voluptates fugiat voluptatum quam. At esse sed quia vitae qui sint neque asperiores. In minus doloribus unde ab qui eum.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(34, 'wp11656089-manga-covers-wallpapers.jpg', 'MocoO-Hardcore Leveling Warrior', 'Hardcore Leveling Warrior', 'Dolorem qui quia nam et unde. Consectetur voluptatibus et dicta. Quidem quia et error. Consectetur nemo perspiciatis sit quae eum itaque.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(35, 'EapfzpZU8AUvNuU.jpg', 'MocoO-Martial Peak', 'Martial Peak', 'Eligendi non ab voluptas ipsam ducimus maxime quisquam. Aliquam quia dolorum vel quae reiciendis quas et. Rem repellat modi aut et tempora.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(36, 'wp11656089-manga-covers-wallpapers.jpg', 'MocoO-Demon Slayer', 'Demon Slayer', 'Sed aspernatur facere modi rerum magnam nobis. Placeat et aut quis rem cumque. Sint magni veniam ipsum provident.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(37, 'evolution-begins-with-a-big-tree-1.webp', 'MocoO-Naruto', 'Naruto', 'Aut delectus qui sequi vel et. Ad aspernatur molestias molestiae id cum. Quam quis sit cum.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(38, 'overgeared.jpg', 'MocoO-Hardcore Leveling Warrior', 'Hardcore Leveling Warrior', 'Dolores est cumque cupiditate. Est enim corporis architecto corrupti. Et corrupti expedita et quia.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(39, 'nano machine.jpg', 'MocoO-Naruto', 'Naruto', 'Quis vero odio ipsum doloribus possimus aut. Aut enim et ea laudantium dolores blanditiis neque. Porro quibusdam officiis et labore id.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(40, 'wp11656054-manga-covers-wallpapers.jpg', 'MocoO-Demon Slayer', 'Demon Slayer', 'Maiores a aliquid deserunt omnis et. Qui nam voluptatem dicta reiciendis incidunt sit culpa. Velit sapiente accusantium ipsum voluptates minus.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(41, 'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg', 'MocoO-Martial Peak', 'Martial Peak', 'Impedit occaecati iste officia deserunt enim. Exercitationem eos maiores tenetur praesentium. Excepturi molestias sunt deserunt.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(42, 'wp11656064-manga-covers-wallpapers.jpg', 'MocoO-Sweet Home', 'Sweet Home', 'Debitis et repellendus aut distinctio ut. Eum pariatur consectetur maiores est dolore non autem ea. Aut voluptas facilis incidunt magnam.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(43, 'solo leveling.png', 'MocoO-Versatile Mage', 'Versatile Mage', 'Nisi maiores ut nihil suscipit et. Sed excepturi facilis molestias maiores quia iste quo.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(44, 'wp10508787-manga-cover-wallpapers.jpg', 'MocoO-One Piece', 'One Piece', 'Odit et dicta iste soluta minus quas. Mollitia velit vitae voluptas numquam alias. Iusto ut atque nemo voluptas facilis quas sunt.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(45, 'solo leveling.png', 'MocoO-Tower of God', 'Tower of God', 'Rerum fugit rerum dolorum dolores. Sint facilis exercitationem et id qui laboriosam perferendis eum. Animi aut aliquid voluptatem dicta ullam.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(46, 'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg', 'MocoO-Versatile Mage', 'Versatile Mage', 'Officia beatae doloremque ut. Necessitatibus iusto ut eveniet blanditiis mollitia magni. Nam voluptas blanditiis vero possimus pariatur nobis.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(47, 'nano machine.jpg', 'MocoO-The King\'s Avatar', 'The King\'s Avatar', 'Exercitationem cupiditate atque suscipit voluptas sint dolore suscipit. Laboriosam quidem qui maxime. Sint qui autem vero expedita voluptas.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(48, 'MV5BZjIyMjE5ZDYtMTQxNC00NTEzLTgwYzYtMmM0NDg3OWFlYWM5XkEyXkFqcGdeQXVyNjMxNzQ2NTQ@._V1_.jpg', 'MocoO-Martial Peak', 'Martial Peak', 'Iste eaque adipisci repellendus fugiat. Dolor facilis optio dolorum optio est veniam ipsum. Similique nostrum earum exercitationem et labore qui.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(49, 'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg', 'MocoO-Solo Leveling', 'Solo Leveling', 'Omnis est ab quibusdam dolorum. Expedita omnis laudantium ad expedita culpa. Nobis quod a et tempora esse commodi.', '2023-08-18 00:02:00', '2023-08-18 00:02:00'),
(50, 'wp10508800-manga-cover-wallpapers.jpg', 'MocoO-Sweet Home', 'Sweet Home', 'Sit delectus quos sed blanditiis. Quaerat dolor quasi repellat qui quo debitis rerum. Incidunt vel qui est.', '2023-08-18 00:02:00', '2023-08-18 00:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE `post_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `post_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(2, 1, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(3, 1, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(4, 2, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(5, 3, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(6, 3, 6, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(7, 4, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(8, 5, 3, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(9, 5, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(10, 6, 1, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(11, 7, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(12, 7, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(13, 7, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(14, 8, 1, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(15, 8, 2, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(16, 8, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(17, 9, 3, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(18, 9, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(19, 10, 3, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(20, 11, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(21, 12, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(22, 13, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(23, 13, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(24, 13, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(25, 14, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(26, 15, 6, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(27, 16, 6, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(28, 16, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(29, 16, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(30, 17, 1, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(31, 17, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(32, 17, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(33, 18, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(34, 18, 6, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(35, 18, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(36, 19, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(37, 20, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(38, 20, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(39, 21, 1, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(40, 21, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(41, 21, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(42, 22, 3, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(43, 22, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(44, 23, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(45, 24, 3, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(46, 24, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(47, 24, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(48, 25, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(49, 26, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(50, 27, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(51, 28, 1, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(52, 28, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(53, 29, 2, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(54, 30, 2, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(55, 30, 3, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(56, 30, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(57, 31, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(58, 31, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(59, 32, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(60, 32, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(61, 32, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(62, 33, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(63, 33, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(64, 34, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(65, 35, 4, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(66, 35, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(67, 35, 9, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(68, 36, 1, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(69, 37, 1, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(70, 37, 7, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(71, 37, 8, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(72, 38, 2, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(73, 38, 5, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(74, 38, 6, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(75, 39, 3, '2023-08-18 00:02:47', '2023-08-18 00:02:47'),
(76, 40, 7, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(77, 40, 9, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(78, 41, 7, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(79, 42, 5, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(80, 42, 8, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(81, 43, 3, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(82, 43, 8, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(83, 43, 9, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(84, 44, 5, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(85, 44, 6, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(86, 44, 9, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(87, 45, 3, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(88, 46, 9, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(89, 47, 8, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(90, 48, 3, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(91, 48, 6, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(92, 48, 7, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(93, 49, 8, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(94, 49, 9, '2023-08-18 00:02:48', '2023-08-18 00:02:48'),
(95, 50, 5, '2023-08-18 00:02:48', '2023-08-18 00:02:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_category_post_id_foreign` (`post_id`),
  ADD KEY `post_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `post_category`
--
ALTER TABLE `post_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `post_category`
--
ALTER TABLE `post_category`
  ADD CONSTRAINT `post_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_category_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
