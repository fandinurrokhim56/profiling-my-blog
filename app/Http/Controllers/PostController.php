<?php

namespace App\Http\Controllers;

use App\Models\PostModel;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\CategoryModel;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        $posts = PostModel::with('categories')->orderByDesc('updated_at')->get();

        return view('PostView/manage-blog', [
            'titlePage' => 'Kelola Blog',
            'posts'     => $posts
        ]);
    }

    protected function getPostsByCategory($categoryName, $limit)
    {
        return PostModel::with('categories')
            ->whereHas('categories', function ($query) use ($categoryName) {
                $query->where('name', $categoryName);
            })
            ->orderBy('created_at', 'desc')
            ->take($limit)
            ->get();
    }

    public function homePageData()
    {
        // mendapatkan 5 postingan terbaru
        $trending = PostModel::with('categories')
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();

        // Mendapatkan data berdasarkan Kategori dan limit data yang diambil
        $sports      = $this->getPostsByCategory('sports', 9);
        $life        = $this->getPostsByCategory('life', 9);
        $programming = $this->getPostsByCategory('programming', 9);
        $tutorial    = $this->getPostsByCategory('tutorial', 4);

        // simpan data diatas kedalam array dan loop kategori kemudian data berdasarkan kategori
        $categories = [$sports, $life, $programming, $tutorial];
        // membatasi panjang limit body
        foreach ($categories as $categoryPosts) {
            foreach ($categoryPosts as $post) {
                $post->body = Str::limit($post->body, 150, '...');
            }
        }

        return view('PostView/home', [
            'titlePage'   => 'Home',
            'trending'    => $trending,
            'sports'      => $sports,
            'life'        => $life,
            'programming' => $programming,
            'tutorial'    => $tutorial
        ]);
    }

    public function newestPosts()
    {
        $posts      = PostModel::with('categories')->orderByDesc('updated_at')->paginate(8);
        $popular    = PostModel::with('categories')->orderBy('title')->take(3)->get();
        $categories = CategoryModel::get();

        // limit body menjadi 150
        foreach ($posts as $post) {
            $post->body = Str::limit($post->body, 150, '...');
        }

        return view('PostView/newest-post', [
            'titlePage'  => 'Terbaru',
            'posts'      => $posts,
            'popular'    => $popular,
            'categories' => $categories
        ]);
    }

    public function showDetails($postSlug, $encodedComicId)
    {
        $postId     = PostModel::decodeId($encodedComicId);
        $popular    = PostModel::with('categories')->orderBy('title')->take(4)->get();
        $categories = CategoryModel::get();

        // Cari komik berdasarkan slug dan yang sudah di decode
        $post = PostModel::where('slug', $postSlug)
            ->where('id', $postId)
            ->firstOrFail();

        return view('PostView/post-details', [
            'titlePage'  => $post->title,
            'post'       => $post,
            'popular'    => $popular,
            'categories' => $categories
        ]);
    }


    public function showAddPostForm()
    {
        $categories = CategoryModel::get();
        if (!$categories) {
            abort(404);
        }

        return view('PostView/add-post', [
            'titlePage'  => 'post.add',
            'categories' => $categories,
        ]);
    }

    // -----------------------------------------------------------------------//
    //                                  Filter                                //
    // -----------------------------------------------------------------------//
    public function search(Request $request)
    {
        $keyword = $request->input('search');

        $posts = PostModel::where('title', 'LIKE', "%$keyword%")->paginate(8);
        $popular = PostModel::with('categories')->orderBy('title')->take(3)->get();

        $count = count($posts);
        return view('partials.search', [
            'titlePage' => 'cari post',
            'posts'     => $posts->appends(request()->except('page')),
            'popular'   => $popular,
            'count'     => $count
        ]);        
    }

    public function showByCategory($name)
    {
        $category = CategoryModel::where('name', $name)->first();
        $popular  = PostModel::with('categories')->orderBy('title')->take(3)->get();

        $posts    = $category->posts()->orderByDesc('created_at')->paginate(8);
        // Limit dari body menjadi 150
        foreach ($posts as $post) {
            $post->body = Str::limit($post->body, 150);
        }

        return view('PostView/category', [
            'titlePage' => 'MocoO-' . $name,
            'posts'     => $posts,
            'popular'   => $popular,
            'category'  => $category
        ]);
    }

    // -----------------------------------------------------------------------//
    //                                  CRUD                                  //
    // -----------------------------------------------------------------------//
    public function createPost(Request $request)
    {
        // DATA VALIDATION
        $validator = Validator::make($request->all(), [
            'image'        => ['image', 'mimes:jpeg,png,jpg,gif,webp', 'max:2048'],
            'title'        => ['required', 'min:3', 'max:50'],
            'categories'   => ['required', 'array'],
            'categories.*' => ['exists:categories,id'],
            'body' => ['required', 'min:12', 'max:1000'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $postData = [
            'title' => ucwords($request->input('title')),
            'body'  => $request->input('body'),
            'slug'  => 'MocoO-' . Str::slug($request->input('title')),
        ];

        // MOVE IMAGE
        if ($request->hasFile('image')) {
            $request->file('image')->move('images/', $request->file('image')->getClientOriginalName());
            $postData['image'] = $request->file('image')->getClientOriginalName();
        }

        $post               = PostModel::create($postData);
        $selectedCategories = $request->input('categories', []);
        $post->categories()->sync($selectedCategories);

        return redirect()->route('blog.manage')->with('success', 'Post berhasil ditambahkan');
    }

    public function editPost($encodePostId)
    {
        $postId     = PostModel::decodeId($encodePostId);
        $post       = PostModel::find($postId);
        $categories = CategoryModel::get();

        return view('PostView/edit-post', [
            'titlePage'  => 'Edit Post',
            'post'       => $post,
            'categories' => $categories
        ]);
    }

    public function updatePost($encodePostId, Request $request)
    {
        // DECODE ID
        $postId = PostModel::decodeId($encodePostId);
        $post   = PostModel::find($postId);

        // VALIDATION
        $validator = Validator::make($request->all(), [
            'image'        => ['image', 'mimes:jpeg,png,jpg,gif,webp', 'max:2048'],
            'title'        => ['required', 'min:3', 'max:50'],
            'categories'   => ['required', 'array'],
            'categories.*' => ['exists:categories,id'],
            'body'         => ['required', 'min:12', 'max:1000'],
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $post->title = ucwords($request->input('title'));
        $post->body = $request->input('body');
        $post->slug = 'MocoO-' . Str::slug($request->input('title'));

        // MOVE IMAGE
        if ($request->hasFile('image')) {
            $request->file('image')->move('images/', $request->file('image')->getClientOriginalName());
            $post->image = $request->file('image')->getClientOriginalName();
        }

        $post->save();

        $selectedCategories = $request->input('categories', []);
        $post->categories()->sync($selectedCategories);

        return redirect()->route('blog.manage')->with('success', 'Post berhasil diupdate');
    }

    public function destroy($id)
    {
        $post = PostModel::findOrFail($id);
        $post->delete();

        return redirect()->route('blog.manage')->with('success', 'Post berhasil dihapus');
    }
    // ---------------------------------------------------------------------------//
    //                                  END CRUD                                  //
    // ---------------------------------------------------------------------------//
}