<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GenreController extends Controller
{
    public static $genres = [
        "Action",
        "Adventure",
        "Comedy",
        "Drama",
        "Fantasy",
        "Historical",
        "Horror",
        "Mystery",
        "Romance",
        "Science Fiction",
        "Slice of Life",
        "Supernatural",
        "Thriller",
        "Sports",
        "Psychological",
        "Mecha",
        "Isekai",
        "Harem",
        "Shounen",
        "Shoujo",
        "Seinen",
        "Josei",
        "Yaoi",
        "Yuri",
    ];
}
