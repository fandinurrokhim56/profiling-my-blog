<?php

namespace App\Models;

use App\Models\PostModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategoryModel extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];

    public function posts()
    {
        return $this->belongsToMany(PostModel::class, 'post_category', 'category_id', 'post_id');
    }
}