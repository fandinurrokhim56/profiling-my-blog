<?php

namespace App\Models;

use App\Models\CategoryModel;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PostModel extends Model
{
    use HasFactory;

    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function encodeId()
    {
        return Hashids::encode($this->id);
    }

    public static function decodeId($encodedId)
    {
        $decodedIds = Hashids::decode($encodedId);
        return isset($decodedIds[0]) ? $decodedIds[0] : null;
    }

    protected $cast = [
        'genre_id' => 'array',
    ];

    public function categories()
    {
        return $this->belongsToMany(CategoryModel::class, 'post_category', 'post_id', 'category_id');
    }
}