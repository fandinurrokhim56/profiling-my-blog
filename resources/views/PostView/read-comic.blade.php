@extends('layouts.main')

@section('container')
    <div class="chapter-content mx-auto text-light">
        <button class="btn btn-primary mb-5" id="backButton">Kembali</button>

        <div class="w-75 mx-auto mt-2">
            <p>Chapter Number: {{ $chapter->title }}</p>
            <p>{{ $chapter->chapter_content }}</p>
        </div>
    </div>

    <script>
        document.getElementById('backButton').addEventListener('click', function() {
            window.history.back();
        });
    </script>
@endsection
