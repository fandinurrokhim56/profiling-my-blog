@extends('layouts.main')

@section('container')
    <div class="section search-result-wrap">
        <div class="container">
            <div class="row">{{ $category->name }}
                <div class="col-12">
                    <div class="heading"></div>
                </div>
            </div>
            <div class="row posts-entry">
                <div class="col-lg-8">
                    @foreach ($posts as $post)
                        <div class="blog-entry d-flex blog-entry-search-item">
                            <div class="w-50 h-100 me-3">
                                <img src="/images/{{ $post->image }}" alt="{{ $post->image }}" width="300"
                                    height="200" class="rounded" style="object-fit: cover;">
                            </div>

                            <div>
                                <span class="date">{{ $post->created_at->format('M. dS, Y') }} &bullet;
                                    @foreach ($post->categories as $category)
                                        <a href="{{ route('category.show', ['name' => $category->name]) }}"
                                            class="badge bg-dark text-light">
                                            {{ $category->name }}
                                        </a>
                                    @endforeach
                                </span>
                                <h2>
                                    <a href="{{ route('post.details', [$post->slug, $post->encodeId()]) }}">
                                        {{ $post->title }}
                                    </a>
                                </h2>
                                <p>{{ $post->body }}</p>
                                <p>
                                    <a href="{{ route('post.details', [$post->slug, $post->encodeId()]) }}"
                                        class="btn btn-sm btn-outline-primary">
                                        Baca Selengkapnya
                                    </a>
                                </p>
                            </div>
                        </div>
                    @endforeach
                    {{-- PAGINATION BUTTON --}}
                    <div class="d-flex justify-content-center mt-4">
                        <ul class="pagination w-25 d-flex justify-content-between">
                            {{-- Previous Page Link --}}
                            @if ($posts->onFirstPage())
                                <li class="page-item disabled">
                                    <span class="btn btn-secondary disabled" aria-hidden="true"><i
                                            class="fas fa-chevron-left"></i></span>
                                </li>
                            @else
                                <li class="page-item">
                                    <a class="btn btn-warning" href="{{ $posts->previousPageUrl() }}" rel="prev"
                                        aria-label="Previous"><i class="fas fa-chevron-left"></i></a>
                                </li>
                            @endif

                            {{-- Page Number and Total Pages --}}
                            @for ($i = 1; $i <= $posts->lastPage(); $i++)
                                <li class="page-item{{ $i === $posts->currentPage() ? ' active' : '' }}">
                                    <a class="page-link" href="{{ $posts->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor

                            {{-- Next Page Link --}}
                            @if ($posts->hasMorePages())
                                <li class="page-item">
                                    <a class="btn btn-warning" href="{{ $posts->nextPageUrl() }}" rel="next"
                                        aria-label="Next">
                                        <i class="fas fa-chevron-right"></i></a>
                                </li>
                            @else
                                <li class="page-item disabled">
                                    <span class="btn btn-secondary disabled" aria-hidden="true"><i
                                            class="fas fa-chevron-right"></i></span>
                                </li>
                            @endif
                        </ul>
                    </div>

                </div>

                <div class="col-lg-4 sidebar">
                    <!-- END sidebar-box -->
                    <div class="sidebar-box">
                        <h3 class="heading">Popular Posts</h3>
                        <div class="post-entry-sidebar">
                            <ul>
                                @foreach ($popular as $item)
                                    <li>
                                        <a href="{{ route('post.details', [$item->slug, $item->encodeId()]) }}">
                                            <img src="/images/{{ $item->image }}" alt="{{ $item->image }}"
                                                class="me-4 rounded">
                                            <div class="text">
                                                <h4>{{ $item->title }}</h4>
                                                <div class="post-meta">
                                                    <span class="mr-2">{{ $item->created_at->format('M. dS, Y') }}
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- END sidebar-box -->
                </div>
            </div>
        </div>
    </div>
@endsection
