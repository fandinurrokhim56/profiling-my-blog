@extends('layouts.main')

{{-- START RETROY LAYOUT --}}
@section('container')
    <div class="row align-items-stretch retro-layout">
        {{-- CARAOUSEL --}}
        <div class="col-md-4">
            <a href="{{ route('post.details', [$trending[0]->slug, $trending[0]->encodeId()]) }}"
                class="h-entry mb-30 v-height gradient">
                {{-- IMAGE --}}
                <div class="featured-img" style="background-image: url('/images/{{ $trending[0]->image }}');"></div>
                {{-- DATE AND TITLE --}}
                <div class="text">
                    <span class="date">{{ $trending[0]->created_at->format('M. dS, Y') }}</span>
                    <h2>{{ $trending[0]->title }}</h2>
                </div>
            </a>

            <a href="{{ route('post.details', [$trending[1]->slug, $trending[1]->encodeId()]) }}"
                class="h-entry v-height gradient">
                {{-- IMAGE --}}
                <div class="featured-img" style="background-image: url('/images/{{ $trending[1]->image }}');"></div>
                {{-- DATE AND TITLE --}}
                <div class="text">
                    <span class="date">{{ $trending[1]->created_at->format('M. dS, Y') }}</span>
                    <h2>{{ $trending[1]->title }}</h2>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ route('post.details', [$trending[2]->slug, $trending[2]->encodeId()]) }}"
                class="h-entry img-5 h-100 gradient">
                {{-- IMAGE --}}
                <div class="featured-img" style="background-image: url('/images/{{ $trending[2]->image }}');"></div>
                {{-- DATE AND TITLE --}}
                <div class="text">
                    <span class="date">{{ $trending[2]->created_at->format('M. dS, Y') }}</span>
                    <h2>{{ $trending[2]->title }}</h2>
                </div>
            </a>
        </div>

        <div class="col-md-4">
            <a href="{{ route('post.details', [$trending[3]->slug, $trending[3]->encodeId()]) }}"
                class="h-entry mb-30 v-height gradient">
                {{-- IMAGE --}}
                <div class="featured-img" style="background-image: url('/images/{{ $trending[3]->image }}');"></div>
                {{-- DATE AND TITLE --}}
                <div class="text">
                    <span class="date">{{ $trending[3]->created_at->format('M. dS, Y') }}</span>
                    <h2>{{ $trending[3]->title }}</h2>
                </div>
            </a>
            <a href="{{ route('post.details', [$trending[4]->slug, $trending[4]->encodeId()]) }}"
                class="h-entry v-height gradient">
                {{-- IMAGE --}}
                <div class="featured-img" style="background-image: url('/images/{{ $trending[4]->image }}');"></div>
                {{-- DATE AND TITLE --}}
                <div class="text">
                    <span class="date">{{ $trending[4]->created_at->format('M. dS, Y') }}</span>
                    <h2>{{ $trending[4]->title }}</h2>
                </div>
            </a>
        </div>
        {{-- END CARAOUSEL --}}
    </div>
@endsection
{{-- END RETROY LAYOUT --}}

{{-- PARTIALS --}}
@include('partials.sports')
@include('partials.programming')
@include('partials.life')
@include('partials.tutorial')
