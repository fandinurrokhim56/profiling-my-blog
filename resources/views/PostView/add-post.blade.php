@extends('layouts.main')

@section('container')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container mt-4">
        <div class="bg-light p-4 rounded-3 shadow-sm p-2">
            <div class="card-header pb-4 bg-primary text-white">
                <h5 class="card-title mx-auto">{{ $titlePage }}</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('post.insert') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{-- TITLE --}}
                    <div class="mb-3">
                        <label for="title" class="form-label">Judul Komik</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>

                    {{-- CATEGORY INPUT WITH RADIO BTN --}}
                    <div class="mb-3">
                        <label for="category" class="form-label">category.show</label>
                        <br>
                        @foreach ($categories as $category)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="categories[]"
                                    id="inlineCheckbox{{ $category->id }}" value="{{ $category->id }}">
                                <label class="badge bg-primary fw-bold"
                                    for="inlineCheckbox{{ $category->id }}">{{ $category->name }}</label>
                            </div>
                        @endforeach
                    </div>

                    {{-- Body Input --}}
                    <div class="mb-3">
                        <label for="body" class="form-label">Konten</label>
                        <textarea class="form-control" id="body" name="body" rows="4" required></textarea>
                    </div>

                    <div class="mb-3">
                        <label for="image" class="form-label">Gambar</label>
                        <input type="file" class="form-control" id="image" name="image">
                    </div>

                    <!-- Image Preview -->
                    <div class="mb-3">
                        <div id="image-preview">
                            <img id="preview-image" class="w-25" src="#" alt="Preview" style="display: none;">
                        </div>
                    </div>

                    {{-- SUBMIT --}}
                    <button type="submit" class="btn btn-primary">Tambah Komik</button>
                </form>
            </div>
        </div>
    </div>

    <script>
        const imageInput = document.getElementById('image');
        const previewImage = document.getElementById('preview-image');

        imageInput.addEventListener('change', function(event) {
            if (event.target.files && event.target.files[0]) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    previewImage.src = e.target.result;
                    previewImage.style.display = 'block';
                };
                reader.readAsDataURL(event.target.files[0]);
            }
        });
    </script>
@endsection
