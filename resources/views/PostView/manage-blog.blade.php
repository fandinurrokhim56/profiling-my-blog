@extends('layouts.main')

@section('search')
    <div class="w-100 mt-5">
        <h2 class="mb-4">{{ $titlePage }}</h2>
        {{-- BTN ADD DATA --}}
        <a href="{{ route('post.add') }}" class="btn btn-primary mb-5">Tambah Post</a>

        {{-- TABLE --}}
        <table class="table border border-5-dark  shadow-sm" id="myTable">
            <thead class="bg-primary text-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Judul</th>
                    <th scope="col">category.show</th>
                    <th scope="col">Dibuat pada</th>
                    <th scope="col">Diubah pada</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                    <tr>
                        {{-- number --}}
                        <th scope="row">{{ $loop->iteration }}</th>
                        {{-- image --}}
                        <td class="w-25">
                            <img src="/images/{{ $post->image }}" alt="{{ $post->title }}"
                                class="img-thumbnail w-50 h-25">
                        </td>
                        {{-- title --}}
                        <td>{{ $post->title }}</td>
                        {{-- categories --}}
                        <td>
                            @foreach ($post->categories as $category)
                                <a href="{{ route('category.show', ['name' => $category->name]) }}"
                                    class="text-decoration-none fw-bold text-light badge bg-secondary me-1 mb-2">
                                    {{ $category->name }}
                                </a>
                            @endforeach
                        </td>

                        {{-- created updated at --}}
                        <td>{{ $post->created_at->format('d M Y') }}</td>
                        <td>{{ $post->updated_at->format('d M Y') }}</td>
                        {{-- action --}}
                        <td class="w-25">
                            {{-- EDIT --}}
                            <a href="{{ route('post.edit', ['encodePostId' => $post->encodeId()]) }}"
                                class="btn btn-sm btn-primary">
                                <i class="bi bi-pencil"></i> Edit
                            </a>

                            {{-- DELETE --}}
                            <form action="{{ route('post.destroy', $post->id) }}" method="POST"
                                class="d-inline delete-form">
                                @csrf
                                @method('DELETE')
                                <button type="button" class="btn btn-sm btn-danger delete-button">
                                    <i class="bi bi-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{-- END TABLE --}}
    </div>
    {{-- PREVIEW IMAGE --}}
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let deleteForms = document.querySelectorAll('.delete-form');

            deleteForms.forEach(function(form) {
                let deleteButton = form.querySelector('.delete-button');

                deleteButton.addEventListener('click', function() {
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Data akan dihapus permanen!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, hapus!',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }
                    });
                });
            });
        });
    </script>
    {{-- END PREVIEW  --}}
@endsection
