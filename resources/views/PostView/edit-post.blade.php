@extends('layouts.main')

@section('search')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container mt-5">
        <div class="bg-light p-4 border border-5-secondary rounded-3 shadow-sm p-2">
            <div class="card-header pb-4 bg-primary text-white">
                <h5 class="card-title mx-auto">{{ $titlePage }}</h5>
            </div>
            <div class="card-body">
                <form action="{{ route('post.update', ['encodePostId' => $post->encodeId()]) }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    {{-- TITLE --}}
                    <div class="mb-3">
                        <label for="title" class="form-label">Judul</label>
                        <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}"
                            required>
                    </div>

                    {{-- CATEGORY INPUT WITH CHECKBOX --}}
                    <div class="form-group">
                        <label for="category">category.show</label><br>
                        @foreach ($categories as $category)
                            <div class="form-check form-check-inline text-light">
                                <input class="form-check-input" type="checkbox" name="categories[]"
                                    id="inlineCheckbox{{ $category->id }}" value="{{ $category->id }}"
                                    {{ $post->categories->contains($category->id) ? 'checked' : '' }}>
                                <label
                                    class="badge bg-primary fw-bold label-checkbox
                                    {{ $post->categories->contains($category->id) ? 'active' : '' }}"
                                    for="inlineCheckbox{{ $category->id }}">{{ $category->name }}</label>
                            </div>
                        @endforeach
                    </div>

                    {{-- Body Input --}}
                    <div class="mb-3">
                        <label for="body" class="form-label">Body</label>
                        <textarea class="form-control" id="body" name="body" rows="4" required>{{ $post->body }}</textarea>
                    </div>

                    {{-- Image Input --}}
                    <div class="mb-3">
                        <label for="image" class="form-label">Gambar</label>
                        <input type="file" class="form-control" id="image" name="image"
                            onchange="previewImage(event)">
                    </div>

                    <!-- Image Preview -->
                    <div class="mb-3">
                        <div id="image-preview">
                            <img src="/images/{{ $post->image }}" class="img-fluid w-25" alt="">
                            <img id="preview-image" class="w-25" src="#" alt="Preview" style="display: none;">
                        </div>
                    </div>

                    {{-- SUBMIT --}}
                    <button type="submit" class="btn btn-primary">Edit Post</button>
                </form>
            </div>

        </div>
    </div>

    {{-- IMAGE PREVIEW --}}
    <script>
        const imageInput = document.getElementById('image');
        const previewImage = document.getElementById('preview-image');

        imageInput.addEventListener('change', function(event) {
            if (event.target.files && event.target.files[0]) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    // Hapus gambar pratinjau yang ada di atas
                    document.querySelector('#image-preview img[src*="{{ $post->image }}"]').remove();

                    previewImage.src = e.target.result;
                    previewImage.style.display = 'block';
                };
                reader.readAsDataURL(event.target.files[0]);
            } else {
                previewImage.src = '';
                previewImage.style.display = 'none';
            }
        });
    </script>

    {{-- BTN CATEGORIES --}}
    <script>
        const checkboxes = document.querySelectorAll('.form-check-input');
        const labels = document.querySelectorAll('.label-checkbox');

        checkboxes.forEach((checkbox, index) => {
            checkbox.addEventListener('change', () => {
                if (checkbox.checked) {
                    labels[index].classList.add('btn-primary');
                    labels[index].classList.remove('btn-outline-secondary');
                } else {
                    labels[index].classList.add('btn-outline-secondary');
                    labels[index].classList.remove('btn-primary');
                }
            });
        });
    </script>
@endsection
