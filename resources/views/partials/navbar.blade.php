<nav class="site-nav">
    <div class="container">
        <div class="menu-bg-wrap">
            <div class="site-navigation">
                <div class="row g-0 align-items-center">
                    {{-- BLOG NAME --}}
                    <div class="col-2">
                        <a href="/" class="logo m-0 float-start">Fandi<span class="text-primary">.</span></a>
                    </div>
                    {{-- SEARCH INPUT --}}
                    <div class="col-8 rounded-2 align-items-center d-flex justify-content-center">
                        <a href="#"
                            class="burger ms-auto float-end site-menu-toggle js-menu-toggle d-inline-block d-lg-none light">
                            <span></span>
                        </a>

                        <form action="{{ route('post.search') }}" method="GET"
                            class="w-75 mx-auto search-form d-none d-lg-inline-block">
                            @csrf
                            <input type="text" class="form-control" name="search" placeholder="Cari...">
                            <span class="bi-search"></span>
                        </form>
                    </div>
                    {{-- LOGIN LOGOUT SECTION --}}
                    <div class="col-2 text-end">
                        @auth
                            <div class="dropdown">
                                <a href="#" class="btn btn-light dropdown-toggle" id="userDropdown" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-user-circle"></i>
                                    {{ Auth::user()->name }}
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="userDropdown">
                                    <li>
                                        <a href="#" class="dropdown-item">Profile</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}" class="dropdown-item"
                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                            style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        @endauth

                        @guest
                            <a href="{{ route('register') }}" class="btn btn-outline-warning">Daftar</a>
                            <a href="{{ route('login') }}" class="btn btn-warning">Masuk</a>
                        @endguest
                    </div>
                </div>
            </div>

            <div class="site-navigation">
                {{-- MENUS --}}
                <div class="col-16 text-center">
                    <form action="#" class="search-form d-inline-block d-lg-none">
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="bi-search"></span>
                    </form>

                    <ul class="js-clone-nav d-none d-lg-inline-block text-start site-menu mx-auto">
                        <li class="{{ $titlePage === 'Home' ? 'active' : '' }}">
                            <a href="/">Home</a>
                        </li>

                        <li class="{{ $titlePage === 'Terbaru' ? 'active' : '' }}">
                            <a href="{{ route('newest.posts') }}">Terbaru</a>
                        </li>
                        @auth
                            <li class="{{ $titlePage === 'Kelola Blog' ? 'active' : '' }}">
                                <a href="{{ route('blog.manage') }}">Kelola blog</a>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
