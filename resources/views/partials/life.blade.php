{{-- POST ENTRY START --}}
@section('life')
    <div class="row mb-4">
        <div class="col-sm-6">
            <h2 class="posts-entry-title">Life</h2>
        </div>
        <div class="col-sm-6 text-sm-end"><a href="{{ route('category.show', ['name' => 'life']) }}" class="read-more">Lihat
                Semua</a></div>
    </div>
    <div class="row g-3">
        <div class="col-md-9">
            <div class="row g-3">
                <div class="col-md-6">
                    <div class="blog-entry">
                        {{-- IMAGE --}}
                        <div class="w-50 h-100 mb-5">
                            <img src="/images/{{ $life[0]->image }}" alt="{{ $life[0]->image }}" width="350"
                                height="200" class="rounded" style="object-fit: cover;">
                        </div>
                        {{-- DATE --}}
                        <span class="date">{{ $life[0]->created_at->format('M. dS, Y') }}</span>
                        {{-- TITLE --}}
                        <h2>
                            <a href="{{ route('post.details', [$life[0]->slug, $life[0]->encodeId()]) }}">
                                {{ $life[0]->title }}
                            </a>
                        </h2>
                        {{-- BODY --}}
                        <p>{{ $life[0]->body }}</p>
                        {{-- BTN --}}
                        <p><a href="{{ route('post.details', [$life[0]->slug, $life[0]->encodeId()]) }}"
                                class="btn btn-sm btn-outline-primary">Baca selengkapnya</a></p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="blog-entry">
                        {{-- IMAGE --}}
                        <div class="w-50 h-100 mb-5">
                            <img src="/images/{{ $life[1]->image }}" alt="{{ $life[1]->image }}" width="350"
                                height="200" class="rounded" style="object-fit: cover;">
                        </div>
                        {{-- DATE --}}
                        <span class="date mt-5">{{ $life[1]->created_at->format('M. dS, Y') }}</span>
                        {{-- TITLE --}}
                        <h2>
                            <a href="{{ route('post.details', [$life[1]->slug, $life[1]->encodeId()]) }}">
                                {{ $life[1]->title }}
                            </a>
                        </h2>
                        {{-- BODY --}}
                        <p>{{ $life[1]->body }}</p>
                        {{-- BTN --}}
                        <p>
                            <a href="{{ route('post.details', [$life[1]->slug, $life[1]->encodeId()]) }}"
                                class="btn btn-sm btn-outline-primary">
                                Baca selengkapnya
                            </a>
                        </p>
                    </div>
                </div>

            </div>
        </div>

        {{-- SIDEBLOG ITEMS --}}
        <div class="col-md-3">
            <ul class="list-unstyled blog-entry-sm">
                <li>
                    {{-- DATE --}}
                    <span class="date">{{ $life[2]->created_at->format('M. dS, Y') }}</span>
                    {{-- TITLE --}}
                    <h3>
                        <a href="{{ route('post.details', [$life[2]->slug, $life[2]->encodeId()]) }}">
                            {{ $life[2]->title }}
                        </a>
                    </h3>
                    {{-- BODY --}}
                    <p>{{ $life[2]->body }}</p>
                    {{-- BTN --}}
                    <p>
                        <a href="{{ route('post.details', [$life[2]->slug, $life[2]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>
                <li>
                    <span class="date">{{ $life[3]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$life[3]->slug, $life[3]->encodeId()]) }}">
                            {{ $life[3]->title }}
                        </a>
                    </h3>

                    <p>{{ $life[3]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$life[3]->slug, $life[3]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>
                <li>
                    <span class="date">{{ $life[4]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$life[4]->slug, $life[4]->encodeId()]) }}">
                            {{ $life[4]->title }}
                        </a>
                    </h3>

                    <p>{{ $life[4]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$life[4]->slug, $life[4]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>

                <li>
                    <span class="date">{{ $life[5]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$life[5]->slug, $life[5]->encodeId()]) }}">
                            {{ $life[5]->title }}
                        </a>
                    </h3>

                    <p>{{ $life[5]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$life[5]->slug, $life[5]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>

            </ul>
        </div>
    </div>
    </div>
@endsection
