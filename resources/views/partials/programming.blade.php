{{-- POST ENTRY START --}}
@section('programming')
    <div class="row mb-4 mt-5">
        <div class="col-sm-6">
            <h2 class="posts-entry-title">Programming</h2>
        </div>
        <div class="col-sm-6 text-sm-end">
            <a href="{{ route('category.show', ['name' => 'programming']) }}" class="read-more">Lihat
                Semua
            </a>
        </div>
    </div>

    <div class="row g-3">
        <div class="col-md-9">
            <div class="row g-3">
                <div class="col-md-6">
                    <div class="blog-entry">
                        {{-- IMAGE --}}
                        <div class="w-50 h-100 mb-5">
                            <img src="/images/{{ $programming[0]->image }}" alt="{{ $programming[0]->image }}"
                                width="350" height="200" class="rounded" style="object-fit: cover;">
                        </div>
                        {{-- DATE --}}
                        <span class="date">{{ $programming[0]->created_at->format('M. dS, Y') }}</span>
                        {{-- TITLE --}}
                        <h2>
                            <a href="{{ route('post.details', [$programming[0]->slug, $programming[0]->encodeId()]) }}">
                                {{ $programming[0]->title }}
                            </a>
                        </h2>
                        {{-- BODY --}}
                        <p>{{ $programming[0]->body }}</p>
                        {{-- BTN --}}
                        <p><a href="{{ route('post.details', [$programming[0]->slug, $programming[0]->encodeId()]) }}"
                                class="btn btn-sm btn-outline-primary">Baca selengkapnya</a></p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="blog-entry">
                        {{-- IMAGE --}}
                        <div class="w-50 h-100 mb-5">
                            <img src="/images/{{ $programming[1]->image }}" alt="{{ $programming[1]->image }}"
                                width="350" height="200" class="rounded" style="object-fit: cover;">
                        </div>
                        {{-- DATE --}}
                        <span class="date mt-5">{{ $programming[1]->created_at->format('M. dS, Y') }}</span>
                        {{-- TITLE --}}
                        <h2>
                            <a href="{{ route('post.details', [$programming[1]->slug, $programming[1]->encodeId()]) }}">
                                {{ $programming[1]->title }}
                            </a>
                        </h2>
                        {{-- BODY --}}
                        <p>{{ $programming[1]->body }}</p>
                        {{-- BTN --}}
                        <p>
                            <a href="{{ route('post.details', [$programming[1]->slug, $programming[1]->encodeId()]) }}"
                                class="btn btn-sm btn-outline-primary">
                                Baca selengkapnya
                            </a>
                        </p>
                    </div>
                </div>

            </div>
        </div>

        {{-- SIDE BLOG ITEMS --}}
        <div class="col-md-3">
            <ul class="list-unstyled blog-entry-sm">
                <li>
                    {{-- DATE --}}
                    <span class="date">{{ $programming[2]->created_at->format('M. dS, Y') }}</span>
                    {{-- TITLE --}}
                    <h3>
                        <a href="{{ route('post.details', [$programming[2]->slug, $programming[2]->encodeId()]) }}">
                            {{ $programming[2]->title }}
                        </a>
                    </h3>
                    {{-- BODY --}}
                    <p>{{ $programming[2]->body }}</p>
                    {{-- BTN --}}
                    <p>
                        <a href="{{ route('post.details', [$programming[2]->slug, $programming[2]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>
                <li>
                    <span class="date">{{ $programming[3]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$programming[3]->slug, $programming[3]->encodeId()]) }}">
                            {{ $programming[3]->title }}
                        </a>
                    </h3>

                    <p>{{ $programming[3]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$programming[3]->slug, $programming[3]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>
                <li>
                    <span class="date">{{ $programming[4]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$programming[4]->slug, $programming[4]->encodeId()]) }}">
                            {{ $programming[4]->title }}
                        </a>
                    </h3>

                    <p>{{ $programming[4]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$programming[4]->slug, $programming[4]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>
            </ul>
        </div>
    </div>
    </div>
@endsection
