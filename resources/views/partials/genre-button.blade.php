<label for="genre" class="form-label">Genre</label>
<br>
<div class="row">
    @foreach ($genres as $key => $genre)
        <div class="col-8 col-sm-6 col-md-5 col-lg-3">
            <div class="form-check form-check-inline">
                <input class="form-check-input btn-check" type="checkbox" name="genre[]"
                    id="btn-check-{{ $key + 1 }}-outlined" value="{{ $key + 1 }}" autocomplete="off">
                    
                <label class="btn btn-outline-dark"
                    for="btn-check-{{ $key + 1 }}-outlined">{{ $genre }}</label>
            </div>
        </div>
    @endforeach
</div>
