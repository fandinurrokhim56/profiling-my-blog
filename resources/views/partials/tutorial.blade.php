@section('tutorial')
    <div class="row mb-4">
        <div class="col-sm-6">
            <h2 class="posts-entry-title">Tutorial</h2>
        </div>
        <div class="col-sm-6 text-sm-end">
            <a href="{{ route('category.show', ['name' => 'tutorial']) }}" class="read-more">
                Lihat Semua
            </a>
        </div>
    </div>

    <div class="row align-items-stretch retro-layout-alt">

        <div class="col-md-5 order-md-2">
            <a href="{{ route('post.details', [$tutorial[0]->slug, $tutorial[0]->encodeId()]) }}" class="hentry img-1 h-100 gradient">
                <div class="featured-img" style="background-image: url('/images/{{ $tutorial[0]->image }}');">
                </div>
                <div class="text">
                    <span>{{ $tutorial[0]->title }}</span>
                    <h2>{{ $tutorial[0]->body }}</h2>
                </div>
            </a>
        </div>

        <div class="col-md-7">

            <a href="{{ route('post.details', [$tutorial[1]->slug, $tutorial[1]->encodeId()]) }}"
                class="hentry img-2 v-height mb30 gradient">
                <div class="featured-img" style="background-image: url('/images/{{ $tutorial[1]->image }}');"></div>
                <div class="text text-sm">
                    <span>{{ $tutorial[1]->title }}</span>
                    <h2>{{ $tutorial[1]->body }}</h2>
                </div>
            </a>

            <div class="two-col d-block d-md-flex justify-content-between">
                <a href="{{ route('post.details', [$tutorial[2]->slug, $tutorial[2]->encodeId()]) }}"
                    class="hentry v-height img-2 gradient">
                    <div class="featured-img" style="background-image: url('/images/{{ $tutorial[2]->image }}');">
                    </div>
                    <div class="text text-sm">
                        <span>{{ $tutorial[2]->title }}</span>
                        <h2>{{ $tutorial[2]->body }}</h2>
                    </div>
                </a>
                <a href="{{ route('post.details', [$tutorial[3]->slug, $tutorial[3]->encodeId()]) }}"
                    class="hentry v-height img-2 ms-auto float-end gradient">
                    <div class="featured-img" style="background-image: url('/images/{{ $tutorial[3]->image }}');">
                    </div>
                    <div class="text text-sm">
                        <span>{{ $tutorial[3]->title }}</span>
                        <h2>{{ $tutorial[3]->body }}</h2>
                    </div>
                </a>
            </div>

        </div>
    </div>
@endsection
