{{-- POST ENTRY START --}}
@section('sports')
    {{-- GENRE --}}
    <div class="row mb-4">
        <div class="col-sm-6">
            <h2 class="posts-entry-title">Sport</h2>
        </div>
        <div class="col-sm-6 text-sm-end">
            <a href="{{ route('category.show', ['name' => 'sports']) }}" class="read-more">Lihat
                Semua
            </a>
        </div>
    </div>

    <div class="row g-3">
        <div class="col-md-9">
            <div class="row g-3">
                <div class="col-md-6">
                    <div class="blog-entry">
                        {{-- IMAGE --}}
                        <div class="w-50 h-100 mb-5">
                            <img src="/images/{{ $sports[0]->image }}" alt="{{ $sports[0]->image }}" width="350"
                                height="200" class="rounded" style="object-fit: cover;">
                        </div>
                        {{-- DATE --}}
                        <span class="date">{{ $sports[0]->created_at->format('M. dS, Y') }}</span>
                        {{-- TITLE --}}
                        <h2>
                            <a href="{{ route('post.details', [$sports[0]->slug, $sports[0]->encodeId()]) }}">
                                {{ $sports[0]->title }}
                            </a>
                        </h2>
                        {{-- BODY --}}
                        <p>{{ $sports[0]->body }}</p>
                        {{-- BTN --}}
                        <p><a href="{{ route('post.details', [$sports[0]->slug, $sports[0]->encodeId()]) }}"
                                class="btn btn-sm btn-outline-primary">Baca selengkapnya
                            </a>
                        </p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="blog-entry">
                        {{-- IMAGE --}}
                        <div class="w-50 h-100 mb-5">
                            <img src="/images/{{ $sports[1]->image }}" alt="{{ $sports[1]->image }}" width="350"
                                height="200" class="rounded" style="object-fit: cover;">
                        </div>
                        {{-- DATE --}}
                        <span class="date mt-5">{{ $sports[1]->created_at->format('M. dS, Y') }}</span>
                        {{-- TITLE --}}
                        <h2>
                            <a href="{{ route('post.details', [$sports[1]->slug, $sports[1]->encodeId()]) }}">
                                {{ $sports[1]->title }}
                            </a>
                        </h2>
                        {{-- BODY --}}
                        <p>{{ $sports[1]->body }}</p>
                        {{-- BTN --}}
                        <p>
                            <a href="{{ route('post.details', [$sports[1]->slug, $sports[1]->encodeId()]) }}"
                                class="btn btn-sm btn-outline-primary">
                                Baca selengkapnya
                            </a>
                        </p>
                    </div>
                </div>

            </div>
        </div>

        {{-- SIDE BLOG ITEMS --}}
        <div class="col-md-3">

            <ul class="list-unstyled blog-entry-sm">
                <li>
                    {{-- DATE --}}
                    <span class="date">{{ $sports[2]->created_at->format('M. dS, Y') }}</span>
                    {{-- TITLE --}}
                    <h3>
                        <a href="{{ route('post.details', [$sports[2]->slug, $sports[2]->encodeId()]) }}">
                            {{ $sports[2]->title }}
                        </a>
                    </h3>
                    {{-- BODY --}}
                    <p>{{ $sports[2]->body }}</p>
                    {{-- BTN --}}
                    <p>
                        <a href="{{ route('post.details', [$sports[2]->slug, $sports[2]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>
                <li>
                    <span class="date">{{ $sports[3]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$sports[3]->slug, $sports[3]->encodeId()]) }}">
                            {{ $sports[3]->title }}
                        </a>
                    </h3>

                    <p>{{ $sports[3]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$sports[3]->slug, $sports[3]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>
                <li>
                    <span class="date">{{ $sports[4]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$sports[4]->slug, $sports[4]->encodeId()]) }}">
                            {{ $sports[4]->title }}
                        </a>
                    </h3>

                    <p>{{ $sports[4]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$sports[4]->slug, $sports[4]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>

                <li>
                    <span class="date">{{ $sports[5]->created_at->format('M. dS, Y') }}</span>
                    <h3>
                        <a href="{{ route('post.details', [$sports[5]->slug, $sports[5]->encodeId()]) }}">
                            {{ $sports[5]->title }}
                        </a>
                    </h3>

                    <p>{{ $sports[5]->body }}</p>

                    <p>
                        <a href="{{ route('post.details', [$sports[5]->slug, $sports[5]->encodeId()]) }}"
                            class="read-more">Continue Reading
                        </a>
                    </p>
                </li>

            </ul>
        </div>
    </div>
    </div>
@endsection
