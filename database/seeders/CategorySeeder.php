<?php

namespace Database\Seeders;

use App\Models\CategoryModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            'Life',
            'Tutorial',
            'Programming',
            'Technology',
            'Health',
            'Travel',
            'Food',
            'Fashion',
            'Sports',
        ];
    
        foreach ($categories as $index => $category) {
            CategoryModel::create([
                'name' => $category,
            ]);
        }
    }    
}
