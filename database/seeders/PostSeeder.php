<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PostModel;
use Database\Factories\PostFactory;

class PostSeeder extends Seeder
{
    public function run()
    {
        PostModel::factory(50)->create();
    }
}

