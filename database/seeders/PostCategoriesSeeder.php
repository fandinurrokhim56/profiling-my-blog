<?php

namespace Database\Seeders;

use App\Models\PostModel;
use App\Models\CategoryModel;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PostCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $posts = PostModel::all();
        $categories = CategoryModel::all();

        foreach ($posts as $post) {
            $randomCategories = $categories->random(rand(1, 3));

            $post->categories()->attach($randomCategories, ['created_at' => now(), 'updated_at' => now()]);
        }
    }

}