<?php

namespace Database\Factories;

use App\Models\PostModel;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostModelFactory extends Factory
{
    protected $model = PostModel::class;

    public function definition()
    {
        $imageOptions = [
            'default.jpeg',
            'evolution-begins-with-a-big-tree-1.webp',
            'nano machine.jpg',
            'overgeared.jpg',
            'solo leveling.png',
            'cover.jpg',
            'Donghua_Season_5_Poster.webp',
            'EapfzpZU8AUvNuU.jpg',
            'Hardcore_Leveling_Warrior_Webtoon_29.webp',
            'Kim_Carnby_and_Hwang_Young-chan_-_Sweet_Home_vol._1_(2020,_Wisdom_House)_book_cover.jpg',
            'Manhua_Cover_II.webp',
            'MV5BZjIyMjE5ZDYtMTQxNC00NTEzLTgwYzYtMmM0NDg3OWFlYWM5XkEyXkFqcGdeQXVyNjMxNzQ2NTQ@._V1_.jpg',
            'Tales-of-Demons-and-Gods.jpg',
            'Tower_of_God_Volume_1_Cover.jpg',
            'wp7988412-naruto-manga-art-wallpapers.jpg',
            'wp10508784-manga-cover-wallpapers.png',
            'wp10508787-manga-cover-wallpapers.jpg',
            'wp10508800-manga-cover-wallpapers.jpg',
            'wp10508875-manga-cover-wallpapers.jpg',
            'wp11656026-manga-covers-wallpapers.jpg',
            'wp11656054-manga-covers-wallpapers.jpg',
            'wp11656064-manga-covers-wallpapers.jpg',
            'wp11656089-manga-covers-wallpapers.jpg',
            'wp11656131-manga-covers-wallpapers.jpg',
        ];        

        $titleOptions = [
            "One Piece",
            "Naruto",
            "Demon Slayer",
            "My Hero Academia",
            "Attack on Titan",
            "Tales of Demons and Gods",
            "The King's Avatar",
            "Battle Through the Heavens",
            "Versatile Mage",
            "Martial Peak",
            "Solo Leveling",
            "Tower of God",
            "The God of High School",
            "Sweet Home",
            "Hardcore Leveling Warrior",
        ];
        
        $selectedTitle = $this->faker->randomElement($titleOptions);
    
        return [
            'image'       => $this->faker->randomElement($imageOptions),
            'slug'        => 'MocoO-' . Str::ucfirst($selectedTitle),
            'title'       => $selectedTitle,
            'body'    => $this->faker->text(150),
        ];
        
    }
}